import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls
import QtQuick.Layouts
import QtMultimedia

Window
{
    width: 640
    height: 480
    visible: true
    title: qsTr("こんにちは世界")


        Rectangle
        {
            id: questionRect
            width: parent.width
            height: parent.height / 3
            border.color: "black"
            border.width: 5
            radius: 10

            Text
            {
                text: "Press Green"
                font.bold: true
                font.pointSize: 24
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        Rectangle
        {
            id: imgRect
            width: parent.width / 3
            height: parent.height / 3
            anchors.top: questionRect.bottom
            anchors.horizontalCenter: questionRect.horizontalCenter

            AnimatedImage
            {
                id: animation
                anchors.fill: parent
                source: "qrc:/imgs/cats-in-space.gif"
            }

        }

        Rectangle
        {
            id: buttomRect
            width: parent.width
            height: parent.height / 3
            anchors.top: imgRect.bottom

            GridLayout
            {
                columns: 2
                anchors.fill: parent
                Button
                {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    flat: true //Removes heihglight when hover

                    Text
                    {
                        id: button_1
                        text: qsTr("Green")
                        color: "yellow"
                        font.pointSize: 24
                        font.bold: true
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 5
                    }

                    background: Rectangle
                    {
                        color: "red"
                        border.width: 1
                        radius: 5
                    }

                    onClicked: console.log(button_1.text + " has been pressed!")
                }
                Button
                {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    flat: true //Removes heihglight when hover

                    Text
                    {
                        id: button_2
                        text: qsTr("Yellow")
                        color: "green"
                        font.pointSize: 24
                        font.bold: true
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 5
                    }

                    background: Rectangle
                    {
                        color: "blue"
                        border.width: 1
                        radius: 5
                    }

                    onClicked: console.log(button_2.text + " has been pressed!")
                }                Button
                {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    flat: true //Removes heihglight when hover

                    Text
                    {
                        id: button_3
                        text: qsTr("Blue")
                        color: "red"
                        font.pointSize: 24
                        font.bold: true
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 5
                    }

                    background: Rectangle
                    {
                        color: "yellow"
                        border.width: 1
                        radius: 5
                    }

                    onClicked: console.log(button_3.text + " has been pressed!")
                }                Button
                {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    flat: true //Removes heihglight when hover

                    Text
                    {
                        id: button_4
                        text: qsTr("Red")
                        color: "blue"
                        font.pointSize: 24
                        font.bold: true
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 5
                    }

                    background: Rectangle
                    {
                        color: "green"
                        border.width: 1
                        radius: 5
                    }

                    onClicked: console.log(button_4.text + " has been pressed!")
                }
            }
        }
}
